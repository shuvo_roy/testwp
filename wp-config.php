<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ']5V}H!]*eF7=YAZgpxwcDA<)HbtOGb{85mX!`@r@(zF-%gpBha.;b_7=3SGsJ8Ep' );
define( 'SECURE_AUTH_KEY',  'NDN-qZ*[0@lS1]]M#8sF[:Yd:mUQ^q``3dqTs|Q1i1&-$Xa6LD*Q?RlWErNqF1OG' );
define( 'LOGGED_IN_KEY',    '_EMqd2Y4w_ik7ciTByr_Fma[F0:i_%Mg{QG4SxW&]=4xpODZ>]h?wXy?(W2[L_lR' );
define( 'NONCE_KEY',        '4h9;Kwo}4xHI[T-z6C(;s6yZ3f}`j$(^,>i|-A87D.$Za})F5Wd8xs*E+G]x7BE2' );
define( 'AUTH_SALT',        'q?^kCk}#4B?gD;J`=aM=F)VqQVJtRIuUD#xQ,!(1NUm0=e8}yk!fLT=%w(~B/Z6e' );
define( 'SECURE_AUTH_SALT', '}:^|a=Kl 1FI?J=&vVA5pA&j)V_;(JjPS8^[vY`C[sYB<*Pxn1#pN}rc~Ogo(.&J' );
define( 'LOGGED_IN_SALT',   'C::=dVMVO4Q4Y IDi@txW`s,-qhXn@d>M(BCON}&64|&-quprm0>Oqp-SvUF4,5r' );
define( 'NONCE_SALT',       'VSt`r9m(TWt[>(jD4B]}h*Sv+|^=D-~Yn^j}<am3=}o;PQ6pEbte:G.!U*3N:Tdq' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
