<?php
use PaulGibbs\WordpressBehatExtension\Context\RawWordpressContext;
use PaulGibbs\WordpressBehatExtension\Context\WordpressContext;
use Behat\Behat\Tester\Exception\PendingException;
use PaulGibbs\WordpressBehatExtension\Context\DashboardContext;
/**
 * Define application features from the specific context.
 */
class FeatureContext extends RawWordpressContext {

    /**
     * Initialise context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the context constructor through behat.yml.
     */
    public function __construct() {
        parent::__construct();
    }
    public function iSwitchTheThemeTo($name)
    {
        $this->switchTheme($name);
    }
}
